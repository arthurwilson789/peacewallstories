//
//  ViewController.swift
//  PeaceWallStories
//
//  Created by Arthur Wilson on 21/03/2021.
//
// when referenceImage spotted, attach child node to anchor node
// when anchorNode out of view, remove child node
// when anchorNode back in view, add child node again



import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    
    var nodeArray: Array<Nodes> = Array()
    var count: Int = 0
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tap))
        sceneView.addGestureRecognizer(tapGestureRecognizer)
        
        let doubleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(doubleTap))
        doubleTapGestureRecognizer.numberOfTapsRequired = 2
        sceneView.addGestureRecognizer(doubleTapGestureRecognizer)
        
        // Set the view's delegate
        sceneView.delegate = self
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if Core.shared.isNewUser() {
            //show onboarding
            guard let vc = storyboard?.instantiateViewController(identifier: "starter_vc") as? Starter else { return }
            vc.modalPresentationStyle = .fullScreen
            vc.modalTransitionStyle = .crossDissolve
            
            present(vc, animated: true)
            Core.shared.setIsNotNewUser()
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        
        //add referenceImages to configuration
        let referenceImages = ARReferenceImage.referenceImages(inGroupNamed: "references", bundle: Bundle.main)
        configuration.detectionImages = referenceImages
        configuration.maximumNumberOfTrackedImages = 1

        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    //loop video function
    func videoLoop(player: AVPlayer) {
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: player.currentItem, queue: nil) {(notification) in
            player.seek(to: CMTime.zero)
            player.play()
            print("Looping Video")
            }
    }
    
    //return SKScene with video player function
    func videoScener(name: String, avPlayer: AVPlayer) -> SKScene {
        
        let size = CGSize(width: 1080, height: 1350)
        let skScene = SKScene(size: size)
        skScene.scaleMode = .aspectFit
        
        let videoSpriteNode = SKVideoNode(avPlayer: avPlayer)
        videoSpriteNode.position = CGPoint(x: size.width/2, y: size.height/2)
        videoSpriteNode.yScale = -1
        skScene.addChild(videoSpriteNode)
        videoLoop(player: avPlayer)
        avPlayer.play()
        
        return skScene
        
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        
        //if the image is a reference image, add the parentnode and child node to node array and give it name
        if let image = anchor as? ARImageAnchor{
            
            let referenceImage = image.referenceImage
            let size = referenceImage.physicalSize
            let name = referenceImage.name
            let player = AVPlayer(url: Bundle.main.url(forResource: name, withExtension: ".mp4")!)
            
            print("found portrait, with name: \(name!)")
            
            let videoScene = videoScener(name: name!, avPlayer: player)
            
            let planeGeometry = SCNPlane(width: size.width, height: size.height)
            
            let material = SCNMaterial()
            material.locksAmbientWithDiffuse = true
            material.isDoubleSided = false
            material.diffuse.contents = videoScene
            
            let plane = SCNNode(geometry: planeGeometry)
            plane.geometry?.materials = [material]
            plane.name = name
            plane.eulerAngles.x = -Float.pi/2
            
            let nodeToAppend = Nodes(name: name!, parentNode: node, childNode: plane, visible: true, time: CMTime.zero, player: player, playing: true)
            
            nodeArray.append(nodeToAppend)
            node.addChildNode(plane)
            
            node.opacity = 0
            let fadeInAction = SCNAction.fadeOpacity(to: 1, duration: 1.5)
            node.runAction(fadeInAction)
            
        }
        
    }
    
    
    //move through nodearray, check if in view. if in view - show, if out of view - stop & delete from parent,
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        
        if nodeArray.count > 0 {
            
            let x = count%nodeArray.count
            
                if let pointOfView = sceneView.pointOfView {
                   
                    let isVisible = sceneView.isNode(nodeArray[x].parentNode, insideFrustumOf: pointOfView)
                    
                    if isVisible && nodeArray[x].visible == false {
                       
                        nodeArray[x].parentNode.isHidden = false
                        nodeArray[x].parentNode.opacity = 0
                        nodeArray[x].player.seek(to: nodeArray[x].time)
                        let fadeInAction = SCNAction.fadeOpacity(to: 1, duration: 1.5)
                        nodeArray[x].parentNode.runAction(fadeInAction)
                        
                        nodeArray[x].visible = true
                        
                    } else if !isVisible && nodeArray[x].visible == true {
                        
                        nodeArray[x].visible = false
                        nodeArray[x].playing = false
                        
                        nodeArray[x].parentNode.opacity = 1
                        let fadeOutAction = SCNAction.fadeOpacity(to: 0, duration: 1.5)
                        nodeArray[x].player.pause()
                        nodeArray[x].time = nodeArray[x].player.currentTime()
                        nodeArray[x].parentNode.runAction(fadeOutAction)
                        nodeArray[x].parentNode.isHidden = true
                        
                    }
                }
            
            count+=1
        }
        
    }
    
    @objc func tap(_ sender: UITapGestureRecognizer){
        
        let tappedView = sender.view as! SCNView
        let touchLocation = sender.location(in: tappedView)
        let hitTest = tappedView.hitTest(touchLocation, options: nil)
        if !hitTest.isEmpty{
            
            let result = hitTest.first!
            let hitName = result.node.name
            print("Tapped \(hitName ?? "empty!")")
            
            let x = nodeArray.firstIndex(where: {$0.name == hitName})!
            
            if nodeArray[x].visible && nodeArray[x].playing == true {
                nodeArray[x].time = nodeArray[x].player.currentTime()
                nodeArray[x].player.pause()
                nodeArray[x].playing = false
            } else if nodeArray[x].visible && nodeArray[x].playing == false {
                nodeArray[x].playing = true;
                nodeArray[x].player.seek(to: nodeArray[x].time)
                nodeArray[x].player.play()
            }
        }
    }
    
    @objc func doubleTap(_ sender: UITapGestureRecognizer){
        
        let tappedView = sender.view as! SCNView
        let touchLocation = sender.location(in: tappedView)
        let hitTest = tappedView.hitTest(touchLocation, options: nil)
        if !hitTest.isEmpty{
            
            let result = hitTest.first!
            let hitName = result.node.name
            print("Double Tapped \(hitName ?? "empty!")")
            
            let x = nodeArray.firstIndex(where: {$0.name == hitName})!
            
            if nodeArray[x].playing {
                nodeArray[x].player.seek(to: CMTime.zero)
                nodeArray[x].time = CMTime.zero
                nodeArray[x].player.play()
            } else {
                nodeArray[x].player.seek(to: CMTime.zero)
                nodeArray[x].playing = true
                nodeArray[x].player.play()
            }
        }
    }
    
    @IBAction func didTapButton() {
        guard let vc = storyboard?.instantiateViewController(identifier: "starter_vc") as? Starter else { return }
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .crossDissolve
        
        present(vc, animated: true)
    }
}

class Core {
    static let shared = Core()
    
    func isNewUser() -> Bool {
        return !UserDefaults.standard.bool(forKey: "isNewUser")
    }
    
    func setIsNotNewUser() {
        UserDefaults.standard.set(true, forKey: "isNewUser")
    }
}

struct Nodes {
    var name: String
    var parentNode: SCNNode
    var childNode: SCNNode
    var visible: Bool
    var time: CMTime
    var player: AVPlayer
    var playing: Bool
}
