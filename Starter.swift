//
//  Starter.swift
//  PeaceWallStories
//
//  Created by Arthur Wilson on 21/03/2021.
//

import UIKit

class Starter: UIViewController {
    
    lazy var view0 = cardView().view()
    lazy var view1 = cardView1().view()
    lazy var view2 = cardView2().view()
    lazy var view3 = cardView3().view()
    
    lazy var views = [view0, view1, view2, view3]
    
    lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.isPagingEnabled = true
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(views.count), height: CGFloat(view.frame.height))//width = views width * number of views
        for i in 0..<views.count {
            scrollView.addSubview(views[i])
            views[i].frame = CGRect(x: view.frame.width*CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
        }
        
        scrollView.delegate = self
        
        return scrollView
    }()
    
    lazy var pageControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.numberOfPages = views.count
        pageControl.currentPage = 0
        pageControl.addTarget(self, action: #selector(pageControlTapHandler(sender:)), for: .touchUpInside)
        
        return pageControl
    }()
    
    @objc func pageControlTapHandler(sender:UIPageControl){
        var frame: CGRect = scrollView.frame
        frame.origin.x = frame.size.width * CGFloat(sender.currentPage)
        scrollView.scrollRectToVisible(frame, animated: true)
    }

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        view.addSubview(scrollView)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        view.addSubview(pageControl)
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        pageControl.heightAnchor.constraint(equalToConstant: 50).isActive = true
        pageControl.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true
        pageControl.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        pageControl.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -12).isActive = true
        
        self.view.backgroundColor = UIColor.black
    }
    
    
    func startButtonAction() {
        print("pressed")
        
        let sampleStoryBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let mainView  = sampleStoryBoard.instantiateViewController(withIdentifier: "main_vc") as! ViewController
        mainView.modalPresentationStyle = .fullScreen
        mainView.modalTransitionStyle = .crossDissolve
        let topController = UIApplication.topViewController()
        print(topController)
        topController!.present(mainView, animated: true, completion: nil)
    }

}
extension Starter: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        let pageIndex = round(scrollView.contentOffset.x / view.frame.width)
        pageControl.currentPage = Int(pageIndex)
    }
}

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.windows.filter {$0.isKeyWindow}.first!.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}
