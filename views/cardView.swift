
import SwiftUI

struct cardView: View {
    
    var body: some View {

        ZStack {
            VStack(spacing: 5) {
                Image("0")
                    .resizable()
                    .scaledToFit()
                    .cornerRadius(20)
                    .padding(.horizontal, 5)
                    .padding(.vertical, 5)
                    .opacity(0.8)
                Text("Peace Walls AR")
                    .foregroundColor(.white)
                    .font(.largeTitle)
                    .fontWeight(.heavy)
                    .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0.15), radius: 2, x: 2, y: 2)

                Text("A project by Stephen Wilson")
                    .foregroundColor(.white)
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 16)
                    .frame(maxWidth: 480)

            } //: VSTACK
        } //: ZSTACK
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity, alignment: .top)
        .background(LinearGradient(gradient: Gradient(colors: [Color.gray, Color.black]), startPoint: .top, endPoint: .bottom))
        .cornerRadius(20)
        .padding(.horizontal, 20)
    }
    
    func view() -> UIView{
        let view = UIHostingController(rootView: self)
        view.view.backgroundColor = UIColor.black
        return view.view
    }
}

//
//
//struct cardView_Previews: PreviewProvider {
//    static var previews: some View {
//        Group {
//            cardView()
//
//        }
//    }
//}
//
